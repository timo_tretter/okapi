/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.CodePeekTranslator.locENUS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class OpenXmlPptxTest{

	@Test
	public void testMaster() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
		params.setTranslatePowerpointMasters(true);
		params.setIgnorePlaceholdersInPowerpointMasters(true);

		OpenXMLFilter filter = new OpenXMLFilter();
		filter.setParameters(params);

		URL url = getClass().getResource("/textbox-on-master.pptx");

		RawDocument doc = new RawDocument(url.toURI(),"UTF-8", locENUS);
		ArrayList<Event> events = getEvents(filter, doc);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("<run1>My title</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<run1>My subtitle</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNotNull(tu);
		assertEquals("<run1>Textbox on layout 1</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 4);
		assertNotNull(tu);
		assertEquals("<run1>Textbox on master</run1>", tu.getSource().toString());

		tu = FilterTestDriver.getTextUnit(events, 5);
		assertNull(tu);
	}

	private ArrayList<Event> getEvents(OpenXMLFilter filter, RawDocument doc) {
		ArrayList<Event> list = new ArrayList<>();
		filter.open(doc, false);
		while (filter.hasNext()) {
			Event event = filter.next();
			list.add(event);
		}
		filter.close();
		return list;
	}
}
