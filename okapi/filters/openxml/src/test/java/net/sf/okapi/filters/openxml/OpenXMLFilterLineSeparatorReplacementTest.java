package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.resource.ITextUnit;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ccudennec
 * @since 23.05.2017
 */
public class OpenXMLFilterLineSeparatorReplacementTest {

    private OpenXMLFilter openXMLFilter = new OpenXMLFilter();

    @Before
    public void before() throws Exception {
        openXMLFilter.getParameters().setAddLineSeparatorCharacter(true);
        openXMLFilter.getParameters().setLineSeparatorReplacement('\u2028');
    }

    @Test
    public void testSimple() throws Exception {
        try (InputStream inputStream = getClass()
                .getResourceAsStream("/Document-with-soft-linebreaks.docx")) {
            openXMLFilter.open(inputStream);
            boolean textUnitFound = false;
            while (openXMLFilter.hasNext()) {
                Event event = openXMLFilter.next();
                System.out.println("Event: " + event);
                if (event.isTextUnit()) {
                    textUnitFound = true;
                    ITextUnit textUnit = event.getTextUnit();
                    assertEquals("First line\u2028second line.",
                            textUnit.getSource().getCodedText());
                    break;
                }
            }

            assertTrue(textUnitFound);
        }
        finally {
            openXMLFilter.close();
        }
    }
}
