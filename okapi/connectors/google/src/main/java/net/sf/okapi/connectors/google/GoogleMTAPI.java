package net.sf.okapi.connectors.google;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;

public interface GoogleMTAPI {
    /**
     * Make a single call to Google to translate the provided list of source texts.  The source texts must
     * obey Google protocol limits of <= 2048 total characters per request.
     * @throws IOException
     * @throws ParseException
     */
    <T> List<TranslationResponse> translate(GoogleQueryBuilder<T> qb)
                throws IOException, ParseException;

    /**
     * Perform translation of oversized (> 2048 characters) segments.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    <T> TranslationResponse translateSingleSegment(GoogleQueryBuilder<T> qb, String sourceText)
                throws IOException, ParseException;

    List<String> getLanguages() throws IOException, ParseException;
}
