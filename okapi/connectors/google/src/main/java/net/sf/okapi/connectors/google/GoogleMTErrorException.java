package net.sf.okapi.connectors.google;

import net.sf.okapi.common.exceptions.OkapiException;

public class GoogleMTErrorException extends OkapiException {
    private final int code;
    private final String message, domain, reason, query;

    public GoogleMTErrorException(int code, String message, String domain, String reason, String query) {
        super(String.format("Error: response code %d - %s", code, message));
        this.code = code;
        this.message = message;
        this.domain = domain;
        this.reason = reason;
        this.query = query;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDomain() {
        return domain;
    }

    public String getReason() {
        return reason;
    }

    public String getQuery() {
        return query;
    }
}
