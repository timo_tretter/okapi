package net.sf.okapi.connectors.google;

public class TranslationResponse {
    private final String source, target;

    TranslationResponse(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }
}
